#Script wrote by Elkin Tenorio and modify by Fabian Salgado
#if you have any question or suggestion please share with us to ek.tenorio@gmail.com or fcsalgador@gmail.com
#This script will find contact zones per genus
#remove all save objects
rm(list=ls())
#load libraries
library(raster)
library(maptools)
library(rgdal)
library(rgeos)

#assign memory to minimum in Mb
ulimit::memory_limit(80000)

#read your file with the columns as follow: Scientific name linked to its shape file name
tabla=read.table('junto.csv', header=F, sep=",", row.names=NULL)
tabla=na.omit(tabla)
#load colombian shapefile
region=shapefile("/home/fsalgado/mapas_hibridos/colombian_shape/COL_adm0.shp") ###Shape Colombia
#load altitude raster
altura=raster('/home/fsalgado/mapas_hibridos/raster/rastera/w001001.adf')
#mask your raster according to your shape

#altura=mask(altura, region)

#altura=aggregate(altura,fact=25,fun=mean)


#Define working directories
#setwd('/Volumes/L-GC_Unix/Mountains/Shapes/All_Shapefiles')
#setwd('/Volumes/LaCie/Birds')

system(paste("mkdir ", getwd(),  "/results", sep=""))

#separate the genus of each species in a different column
tabla[,1]<-as.character(tabla[,1])
A=strsplit(tabla[,1], " ")
AA=c(); for(i in 1:length(A)){AA=c(AA, A[i][[1]][1])}
tabla=cbind(AA, tabla)


#use your raster as dimension file
Tabla=as.data.frame(altura)

#Extract the genus names
genus<-levels(tabla[,1])

# this loop is going to make all the magic!
#look around each genus

pdf("results/Hibridos_backup.pdf")

for (i in 1:length(genus))


{
	print(i) #print the genus just to check

	AAA=subset(tabla, AA==genus[i]) #make the subset of each genus

	AAA=AAA[!duplicated(AAA), ] #remove all the duplicates


	if(dim(AAA)[1]<=1){next} #if the genus has only one species remove it

 #if it has more than then see if they itercept at some point of the distribution
	if(dim(AAA)[1]>1){

	mapas=AAA[,3] #take the name of the shape files

	combinaciones=combn(mapas, 2)

	listaIntersecciones=vector("list", dim(combinaciones)[2])

	for (j in 1:dim(combinaciones)[2]) #this loop is going to check if there is an intercept between the species of the genus

		{

			Map1=rasterize(shapefile(as.character(combinaciones[,j][1])), altura)
			Map2=rasterize(shapefile(as.character(combinaciones[,j][2])), altura)
			listaIntersecciones[j] <- overlay(Map1, Map2, fun=mean)
			#listaIntersecciones[j] <- intersect(gBuffer(shapefile(as.character(combinaciones[,j][1])),width=0), gBuffer(shapefile(as.character(combinaciones[,j][2])),width=0))

		}

			for (k in 1:length(listaIntersecciones))
			{
				Tabla=cbind(Tabla, as.data.frame(listaIntersecciones[[k]]))
				plot(altura, main=paste(as.character(combinaciones[,k][1]), as.character(combinaciones[,k][2]), 				sep="  X  "))
				plot(shapefile(as.character(combinaciones[,k][1])), col=rgb(0,0,1.0, alpha=0.4), add=T)
				plot(shapefile(as.character(combinaciones[,k][2])), col=rgb(1,0,0, alpha=0.4), add=T)
			}

	}

}

dev.off()

save(list=ls(), file=paste(paste(getwd(),"/results/resultados_objetos",".rda",sep="")))

pdf("results/HibridosConocidosColombia.pdf")
Tabla=Tabla[,-1]
SumTabla=rowSums(Tabla, na.rm=T)

Plantilla=altura

values(Plantilla)<-SumTabla

plot(Plantilla, xlim=c(-80, -60), ylim=c(-9, 15))
plot(region, add=T)





##############


Tabla=matrix()

for (j in 1:length(listaIntersecciones))


{
	Map=listaIntersecciones[j]

	if(is.null(Map[[1]])) {next}

	if(!is.null(Map[[1]]))

	{

	Map=rasterize(Map[[1]], altura, field=1)

	Tabla=cbind(Tabla, as.data.frame(Map))

	}

}



Tabla=Tabla[,-1]
SumTabla=rowSums(Tabla, na.rm=T)

Plantilla=altura

values(Plantilla)<-SumTabla

Plantilla[which(values(Plantilla)<3)]<-NA
Plantilla[!is.na(values(Plantilla))]<-1

plot(Plantilla, xlim=c(-80, -60), ylim=c(-9, 15)); plot(region, add=T)

dev.off()

save(list=ls(), file=paste(paste(getwd(),"/results/resultados_objetos2",".rda",sep="")))
