#Script wrote by Elkin Tenorio and Fabian Salgado
#if you have any question or suggestion please shae with us ek.tenorio@gmail.com or fcsalgador@gmail.com
#This script will select all species that have distribution in colombia

#Remove all your memory objects
rm(list=ls())

#load libraries
library(raster)
library(maptools)
library(rgdal)
library(rgeos)


#read colombian shape to set limits
region=shapefile("/home/fsalgado/mapas_hibridos/colombian_shape/COL_adm0.shp")
#change your directory
setwd('/home/fsalgado/mapas_hibridos/aves')
#Load your shapes to compare with the coolombian map
listaMapas<-list.files("/home/fsalgado/mapas_hibridos/aves", pattern="shp")

#This is the loop that makes all the magic!! :D

intersecMapas<-function(x)

{
	mapa=shapefile(x)

	AA=intersect(region, mapa)

	if(length(AA)==0) {data=NA}

	if (length(AA)!=0) {data=x}

	return(data)

}


#this will use half the cores of your computer to run the loop above
prueba=parallel::mclapply(listaMapas, intersecMapas, mc.cores=parallel::detectCores()/2)

#put the species that are in colombia in a single dataframe
nombres=data.frame(unlist(prueba))
nombres=na.omit(nombres)
write.table(x=nombres, file="colombian_species.txt")
